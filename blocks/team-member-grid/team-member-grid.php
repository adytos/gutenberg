<?php 
$team_group_grid = get_field("team_group_grid"); 
$numbers = get_field("numbers_of_columns");

$name = get_field( 'name' );
$position = get_field( 'position' );
$description = get_field( 'description' );

$phone_number = get_field( 'phone_number' );
$mail = get_field( 'mail' );

$visibility = get_field("display_position");





// Support custom "anchor" values.
$anchor = '';
if ( ! empty( $block['anchor'] ) ) {
    $anchor = 'id="' . esc_attr( $block['anchor'] ) . '" ';
}

// Create class attribute allowing for custom "className" and "align" values.
$class_name = 'member';
if ( ! empty( $block['className'] ) ) {
    $class_name .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
    $class_name .= ' align' . $block['align'];
}


$grid_col = '';
if($numbers == '2') {
$grid_col .= '2';
} elseif ($numbers == '3'){
$grid_col .= '3';
}
else {
    $grid_col .= '4';
} 

$vis = '';
if($visibility == 'hidden') {
$vis .= 'hidden';
} 
else {
    $vis .= 'display';
} 


?>               
    
<section <?php echo $anchor; ?>class="<?php echo esc_attr( $class_name ); ?>">
    <div class="container">
        <h2>Všetci členovia</h2>
    
        <div class="members-item__group grid-cols-<?php echo $grid_col; ?>">

            <?php  foreach ($team_group_grid as $post) : ?>
                <div class="members-item">
                    <div class="members-item__img">
                        <?php $image = get_field('profile_image', $post->ID); ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                    </div>
                    <p class="members-item__position <?php echo $vis;?>">
                        <?php echo $post->position?>
                    </p>
                    <p class="members-item__name">
                        <?php echo $post->name?>
                    </p>
                    <p class="members-item__phone">
                        <?php echo $post->phone_number?>
                    </p>

            </div>     
            <?php endforeach;?>
        </div>
    </div>
</section>  
 