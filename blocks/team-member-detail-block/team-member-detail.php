<?php

$name = get_field( 'name' );
$position = get_field( 'position' );
$description = get_field( 'description' );
$phone_number = get_field( 'phone_number' );
$mail = get_field( 'mail' );



$relations = get_field("team_member_single");
$image = get_field_object('profile_image', $relations);

?>

<h2>
    Zobrazuje sa jeden team member vybrany v admine: <?php the_title()?>
</h2>
<div class="team-member__detail">
    <?php if(!empty($image)){ ?>
    <div class="team-member__detai-img">
        <img src="<?php echo $image["value"]["url"]?>" >   
    </div>
    <?php }?>

    <?php if(!empty($relations->description)){ ?>
    <p class="team-member__detail-desc">
        <?php echo $relations->description;?>
    </p>
    <?php }?>
    
    <?php if(!empty($relations->name)){ ?>
    <p class="team-member__detail-name">
        <?php echo $relations->name;?>
    </p>
    <?php }?>

    <?php if(!empty($relations->mail)){ ?>
    <p class="team-member__detail-mail">
        <i class="fas fa-envelope"></i><?php ?> <?php echo $relations->mail;?>
    </p>
    <?php }?>
    
    <?php if(!empty($relations->phone_number)){ ?>
    <p class="team-member__detail-phone">
        <i class="fas fa-phone-alt"></i> <?php echo $relations->phone_number;?>
    </p>
    <?php }?>
</div>