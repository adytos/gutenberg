<?php


//Create new post type in admin menu

add_action('init', 'create_post_type');
function create_post_type()
{
	register_post_type(
		'team_members',
		array(
			
			'labels' => array(
				'name' => __('Team Members'),
				'singular_name' => __('Team Members'),
				'add_new' => __('Pridať '),
				'add_new_item' => __('Pridať ')
			),
			'supports' => array( 'thumbnail','title' ),
			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-hammer',
			//'supports' => false,
			'menu_position' => 5,
		)
	);
	
}	
 //register custom ACF blok
 add_action( 'init', 'register_acf_blocks' );
 function register_acf_blocks() {
     register_block_type( get_stylesheet_directory( ) . '/blocks/team-member-detail-block' );
	 register_block_type( get_stylesheet_directory( ) . '/blocks/team-member-grid' );
	
 }

 

